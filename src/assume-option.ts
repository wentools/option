import { none } from './none.ts'
import { some } from './some.ts'
import { Option } from './option.ts'

type RemoveObject<T> = T extends unknown
  ? keyof T extends never
    ? never
    : T
  : never

/** Creates an `Option` from a type that is possibly undefined or null */
const assumeOption = <DataType>(
  data: DataType | undefined | null
): Option<RemoveObject<DataType>> =>
  data === undefined || data === null
    ? none()
    : some(data as RemoveObject<DataType>)

/** Creates an `Option` from a type that is possibly undefined or null */
const assumeOptionAsync = async <DataType>(
  data: Promise<DataType | undefined | null>
): Promise<Option<RemoveObject<Awaited<DataType>>>> => {
  const awaitedData = await data

  return awaitedData === undefined || data === null
    ? none()
    : some(awaitedData as RemoveObject<Awaited<DataType>>)
}

export { assumeOption, assumeOptionAsync }
