import { addBoolMethods } from './add-bool-methods.ts'
import { Option } from './option.ts'

const none = <DataType>(): Option<DataType> => {
  return {
    unwrap: () => {
      throw new Error(
        "No data here. Don't use unwrap unless you know it's completely safe."
      )
    },
    unwrapOr: (or) => or,
    unwrapOrElse: (orElse) => orElse(),
    unwrapOrElseAsync: async (orElse) => await orElse(),
    match: ({ none: _none }) => _none(),
    map: () => none(),
    mapOr: (_, or) => or,
    mapOrElse: (_, orElse) => orElse(),
    filter: () => none(),
    filterAsync: async () => none(),
    zip: () => none(),
    and: () => none(),
    andThen: () => none(),
    andThenAsync: async () => none(),
    or: (option) => option,
    orElse: (fn) => fn(),
    contains: () => false,
    toString: () => 'none()',
    optOut: () => null,
    ...addBoolMethods(false),
  }
}

export { none }
