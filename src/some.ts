import { addBoolMethods } from './add-bool-methods.ts'
import { Option } from './option.ts'
import { none } from './none.ts'

const some = <DataType>(data: DataType): Option<DataType> => {
  return {
    unwrap: () => data,
    unwrapOr: () => data,
    unwrapOrElse: () => data,
    unwrapOrElseAsync: async () => data,
    match: ({ some: _some }) => _some(data),
    map: (fn) => some(fn(data)),
    mapOr: (fn) => fn(data),
    mapOrElse: (fn) => fn(data),
    filter: (fn) => (fn(data) ? some(data) : none()),
    filterAsync: async (fn) =>
      (await fn(data)) ? some(data) : none(),
    zip: (secondData) => secondData.map((x) => [data, x]),
    and: (option) => option,
    andThen: (fn) => fn(data).map((_data) => _data),
    andThenAsync: async (fn) =>
      (await fn(data)).map((_data) => _data),
    or: () => some(data),
    orElse: () => some(data),
    contains: (secondData) => data === secondData,
    toString: () =>
      `some(${
        (data as unknown as Option<unknown>)?.unwrapOrElse !==
        undefined
          ? (data as unknown as Option<unknown>).toString()
          : data
      })`,
    optOut: () => data,
    ...addBoolMethods(true),
  }
}

export { some }
