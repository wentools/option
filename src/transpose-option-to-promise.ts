import { Option } from './option.ts'
import { some } from './some.ts'
import { none } from './none.ts'

const transposeOptionToPromise = async <DataType>(
  option: Option<Promise<DataType>>
): Promise<Option<DataType>> =>
  option.mapOrElse(
    async (d) => some(await d),
    async () => none<DataType>()
  )

export { transposeOptionToPromise }
