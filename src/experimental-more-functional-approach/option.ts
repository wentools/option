enum OptionType {
  Some = 'Some',
  None = 'None',
}

interface Option<DataType> {
  optionType: OptionType
  _data: null | DataType
}

export { OptionType }
export type { Option }
