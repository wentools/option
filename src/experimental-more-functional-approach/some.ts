import { Option, OptionType } from './option.ts'

const some = <DataType>(data: DataType): Option<DataType> => {
  return {
    optionType: OptionType.Some,
    _data: data,
  }
}

export { some }
