import { isSome } from './is-some.ts'
import { none } from './none.ts'
import { Option } from './option.ts'
import { some } from './some.ts'
import { unwrap } from './unwrap.ts'

const filter = <DataType>(predicate: (data: DataType) => boolean) => (
  option: Option<DataType>
): Option<DataType> =>
  isSome(option)
    ? predicate(unwrap(option))
      ? some(unwrap(option))
      : none()
    : none()

export { filter }
