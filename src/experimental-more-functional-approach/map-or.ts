import { isSome } from './is-some.ts'
import { Option } from './option.ts'
import { unwrap } from './unwrap.ts'

const mapOr = <DataType, FnReturnType>(
  fn: (data: DataType) => FnReturnType,
  or: FnReturnType,
  option: Option<DataType>
): FnReturnType => (isSome(option) ? fn(unwrap(option)) : or)

export { mapOr }
