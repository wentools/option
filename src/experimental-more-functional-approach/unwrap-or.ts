import { isSome } from './is-some.ts'
import { Option } from './option.ts'
import { unwrap } from './unwrap.ts'

const unwrapOr = <DataType>(
  or: DataType,
  option: Option<DataType>
): DataType => (isSome(option) ? unwrap(option) : or)

export { unwrapOr }
