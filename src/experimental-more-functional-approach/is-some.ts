import { Option, OptionType } from './option.ts'

const isSome = <DataType>(option: Option<DataType>): boolean =>
  option.optionType === OptionType.Some

export { isSome }
