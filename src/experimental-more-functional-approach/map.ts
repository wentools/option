import { isSome } from './is-some.ts'
import { none } from './none.ts'
import { Option } from './option.ts'
import { some } from './some.ts'
import { unwrap } from './unwrap.ts'

const map = <DataType, FnReturnType>(
  fn: (data: DataType) => FnReturnType,
  option: Option<DataType>
): Option<FnReturnType> =>
  isSome(option) ? some(fn(unwrap(option))) : none()

export { map }
