import { isSome } from './is-some.ts'
import { map } from './map.ts'
import { none } from './none.ts'
import { Option } from './option.ts'
import { unwrap } from './unwrap.ts'

const zip = <DataType, SecondDataType>(
  secondOption: Option<SecondDataType>,
  option: Option<DataType>
): Option<[DataType, SecondDataType]> =>
  isSome(option)
    ? map((x) => [unwrap(option), x], secondOption)
    : none()

export { zip }
