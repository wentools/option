import { Option, OptionType } from './option.ts'

const none = <DataType>(): Option<DataType> => {
  return {
    optionType: OptionType.Some,
    _data: null,
  }
}

export { none }
