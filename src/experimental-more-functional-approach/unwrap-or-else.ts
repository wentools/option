import { isSome } from './is-some.ts'
import { Option } from './option.ts'
import { unwrap } from './unwrap.ts'

const unwrapOrElse = <DataType>(
  orElse: () => DataType,
  option: Option<DataType>
): DataType => (isSome(option) ? unwrap(option) : orElse())

export { unwrapOrElse }
