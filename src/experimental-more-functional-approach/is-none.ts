import { Option, OptionType } from './option.ts'

const isNone = <DataType>(option: Option<DataType>): boolean =>
  option.optionType === OptionType.None

export { isNone }
