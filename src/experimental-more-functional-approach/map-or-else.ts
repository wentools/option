import { isSome } from './is-some.ts'
import { Option } from './option.ts'
import { unwrap } from './unwrap.ts'

const mapOrElse = <DataType, FnReturnType>(
  fn: (data: DataType) => FnReturnType,
  orElse: () => FnReturnType,
  option: Option<DataType>
): FnReturnType => (isSome(option) ? fn(unwrap(option)) : orElse())

export { mapOrElse }
