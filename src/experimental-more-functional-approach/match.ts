import { mapOrElse } from './map-or-else.ts'
import { Option } from './option.ts'

const match = <DataType, MatchExpressionType>(
  {
    some: _some,
    none: _none,
  }: {
    some(data: DataType): MatchExpressionType
    none(): MatchExpressionType
  },
  option: Option<DataType>
) => mapOrElse(_some, _none, option)

export { match }
