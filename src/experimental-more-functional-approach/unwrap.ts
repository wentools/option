import { Option, OptionType } from './option.ts'

const unwrap = <DataType>(option: Option<DataType>): DataType => {
  if (option.optionType === OptionType.None) {
    throw Error(
      "No data here. Don't use unwrap unless you know it's completely safe."
    )
  }

  return option._data as DataType
}

export { unwrap }
